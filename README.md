# Biblioteca de validaciones para la cadena de bloques estatal Boliviana

Colección de funciones para consultar a la cadena de bloques.

## Inicialización

```javascript
// instanciar
let blockchainVerificador = require("@rmgarcia/verificador-blockchain-bo")("<archivo de configuracion>");

// El <archivo de configuracion> debe tener la estructura que se proporciona en el archivo config.sample.js

```
Ejemplo de archivo de configuración:

```javascript
module.exports = {
  user_id: "user@algo.gob.bo", // usuario enrolado (opcional)
  msp_id: "MSP id" , // cambiar
  channel_id: "canal", // canal de la cadena de blqoues
  query_user: "userc", // usuario que hace las consultas
  write_user: "usere", // usuario que puede hacer modificaciones (opcional)
  chaincode0: { // datos del chaincode
    chaincode_id: "chaincodeID",
    fcns: {
      introducir: "createDoc",
      consultar: "queryDoc"
    }
  },
  peers: [ // array de pares o nodos
    {
      name: "peer0.agetic.gob.bo",
      desc: "",
      url: "grpcs://<ip>:<puerto_peer0>",
      ip_and_port: "<ip>:<puerto_peer0>",
      event_url: "grpcs://<ip>:<puerto_peer>",
      peso: 0,
      privateKeyFolder: "<ruta>/crypto-config/peerOrganizations/agetic.gob.bo/peer0.agetic.gob.bo/msp/keystore",
      signedCert: "<ruta>/crypto-config/peerOrganizations/agetic.gob.bo/peer0.agetic.gob.bo/msp/admincerts/Admin@agetic.gob.bo-cert.pem",
      peer_tls_cacerts: "<ruta>/crypto-config/peerOrganizations/agetic.gob.bo/peer0.agetic.gob.bo/msp/tlscacerts/tlsca.agetic.gob.bo-cert.pem",
      orderer0_tls_cacerts: "<ruta>/crypto-config/ordererOrganizations/gob.bo/orderers/orderer0.gob.bo/tls/ca.crt",
      orderer1_tls_cacerts: "<ruta>/crypto-config/ordererOrganizations/gob.bo/orderers/orderer1.gob.bo/tls/ca.crt",
      server_hostname: "peer0.agetic.gob.bo"
    },
    {
      name: "peer1.agetic.gob.bo",
      desc: "",
      url: "grpcs://<ip>:<puerto_peer0>",
      ip_and_port: "<ip>:<puerto_peer0>",
      event_url: "grpcs://<ip>:<puerto_peer>",
      peso: 0,
      privateKeyFolder: "<ruta>/crypto-config/peerOrganizations/agetic.gob.bo/peer1.agetic.gob.bo/msp/keystore",
      signedCert: "<ruta>/crypto-config/peerOrganizations/agetic.gob.bo/peer1.agetic.gob.bo/msp/admincerts/Admin@agetic.gob.bo-cert.pem",
      peer_tls_cacerts: "<ruta>/crypto-config/peerOrganizations/agetic.gob.bo/peer1.agetic.gob.bo/msp/tlscacerts/tlsca.agetic.gob.bo-cert.pem",
      orderer0_tls_cacerts: "<ruta>/crypto-config/ordererOrganizations/gob.bo/orderers/orderer0.gob.bo/tls/ca.crt",
      orderer1_tls_cacerts: "<ruta>/crypto-config/ordererOrganizations/gob.bo/orderers/orderer1.gob.bo/tls/ca.crt",
      server_hostname: "peer1.agetic.gob.bo"
    },
  ],
  orderers: [ // array de orderers
    {
      name: "orderer0",
      url: "grpcs://<ip>:<puerto>",
      tls_ca_certs: "<ruta>/crypto-config/ordererOrganizations/gob.bo/orderers/orderer0.gob.bo/tls/ca.crt"
    }
  ],
  cerficateAuthorities: { // datos autoridad certificadora
    "ca.gob.bo": "https://<ip>:<puerto>",
    "tlsCACerts": "<ruta>/crypto-config/peerOrganizations/agetic.gob.bo/ca/ca.agetic.gob.bo-cert.pem",
    "registrar": {
      "enrollId": "<usuario_enrolado>",
      "enrollSecret": "<secret>"
    },
    "caName": "ca.gob.bo"
  }
};

```
Se buscarán los archivos que se indican en el archivo de configuración, en el ejemplo de arriba se guardan credenciales en las carpetas `crypto-config` y `hfc-key-store`.
