module.exports = {
  user_id: "user@algo.gob.bo", // usuario enrolado (opcional)
  msp_id: "MSP id" , // cambiar
  channel_id: "canal", // canal de la cadena de blqoues
  query_user: "userc", // usuario que hace las consultas
  write_user: "usere", // usuario que puede hacer modificaciones (opcional)
  chaincode0: { // datos del chaincode
    chaincode_id: "chaincodeID",
    fcns: {
      introducir: "createDoc",
      consultar: "queryDoc"
    }
  },
  peers: [ // array de pares o nodos
    {
      name: "peer0.agetic.gob.bo",
      desc: "",
      url: "grpcs://<ip>:<puerto_peer0>",
      ip_and_port: "<ip>:<puerto_peer0>",
      event_url: "grpcs://<ip>:<puerto_peer>",
      peso: 0,
      privateKeyFolder: "<ruta>/crypto-config/peerOrganizations/agetic.gob.bo/peer0.agetic.gob.bo/msp/keystore",
      signedCert: "<ruta>/crypto-config/peerOrganizations/agetic.gob.bo/peer0.agetic.gob.bo/msp/admincerts/Admin@agetic.gob.bo-cert.pem",
      peer_tls_cacerts: "<ruta>/crypto-config/peerOrganizations/agetic.gob.bo/peer0.agetic.gob.bo/msp/tlscacerts/tlsca.agetic.gob.bo-cert.pem",
      server_hostname: "peer0.agetic.gob.bo"
    },
    {
      name: "peer1.agetic.gob.bo",
      desc: "",
      url: "grpcs://<ip>:<puerto_peer0>",
      ip_and_port: "<ip>:<puerto_peer0>",
      event_url: "grpcs://<ip>:<puerto_peer>",
      peso: 0,
      privateKeyFolder: "<ruta>/crypto-config/peerOrganizations/agetic.gob.bo/peers/peer1.agetic.gob.bo/msp/keystore",
      signedCert: "<ruta>/crypto-config/peerOrganizations/agetic.gob.bo/peers/peer1.agetic.gob.bo/msp/admincerts/Admin@agetic.gob.bo-cert.pem",
      peer_tls_cacerts: "<ruta>/crypto-config/peerOrganizations/agetic.gob.bo/peers/peer1.agetic.gob.bo/msp/tlscacerts/tlsca.agetic.gob.bo-cert.pem",
      server_hostname: "peer1.agetic.gob.bo"
    },
    {
      name: "peer2.agetic.gob.bo",
      desc: "",
      url: "grpcs://<ip>:<puerto_peer0>",
      ip_and_port: "<ip>:<puerto_peer0>",
      event_url: "grpcs://<ip>:<puerto_peer0>",
      peso: 0,
      privateKeyFolder: "<ruta>/crypto-config/peerOrganizations/agetic.gob.bo/peers/peer2.agetic.gob.bo/msp/keystore",
      signedCert: "<ruta>/crypto-config/peerOrganizations/agetic.gob.bo/peers/peer2.agetic.gob.bo/msp/admincerts/Admin@agetic.gob.bo-cert.pem",
      peer_tls_cacerts: "<ruta>/crypto-config/peerOrganizations/agetic.gob.bo/peers/peer2.agetic.gob.bo/msp/tlscacerts/tlsca.agetic.gob.bo-cert.pem",
      server_hostname: "peer2.agetic.gob.bo"
    },
    {
      name: "peer3.agetic.gob.bo",
      desc: "",
      url: "grpcs://<ip>:<puerto_peer0>",
      ip_and_port: "<ip>:<puerto_peer0>",
      event_url: "grpcs://<ip>:<puerto_peer0>",
      peso: 0,
      privateKeyFolder: "<ruta>/crypto-config/peerOrganizations/agetic.gob.bo/peers/peer3.agetic.gob.bo/msp/keystore",
      signedCert: "<ruta>/crypto-config/peerOrganizations/agetic.gob.bo/peers/peer3.agetic.gob.bo/msp/admincerts/Admin@agetic.gob.bo-cert.pem",
      peer_tls_cacerts: "<ruta>/crypto-config/peerOrganizations/agetic.gob.bo/peers/peer3.agetic.gob.bo/msp/tlscacerts/tlsca.agetic.gob.bo-cert.pem",
      server_hostname: "peer3.agetic.gob.bo"
    }
  ],
  orderers: [ // array de orderers
    {
      name: "orderer0",
      url: "grpcs://<ip>:<puerto>",
      tls_ca_certs: "<ruta>/crypto-config/ordererOrganizations/gob.bo/orderers/orderer0.gob.bo/tls/ca.crt"
    },
    {
      name: "orderer1",
      url: "grpcs://<ip>:<puerto_peer0>",
      tls_ca_certs: "<ruta>/crypto-config/ordererOrganizations/gob.bo/orderers/orderer1.gob.bo/tls/ca.crt"
    }
  ],
  certificateAuthorities: { // datos autoridad certificadora
    "ca.gob.bo": "https://<ip>:<puerto>",
    "tlsCACerts": "<ruta>/crypto-config/crypto-config/ordererOrganizations/gob.bo/ca/ca.gob.bo-cert.pem",
    "registrar": {
      "enrollId": "<usuario_enrolado>",
      "enrollSecret": "<secret>"
    },
    "caName": "ca.gob.bo"
  }
};
