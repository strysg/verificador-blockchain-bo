'use strict';
/**
 * @fileOverview Conjunto de operaciones para interactuar con el registro de orden cronológico y temporalidad, para el servicio de certificación con ciudadanía.
 * @name operaciones.js
 * @author Rodrigo Garcia <rgarcia@agetic.gob.bo> (2019)
 * @license LPG-Bolivia
 */


var Fabric_Client = require('fabric-client');
var path = require('path');
var util = require('util');
var fs = require('fs');
var os = require('os');

const moment = require('moment');

/**
 * Carga archivos de configuraciones y parametros
 * @param{string} configFile: Archivo de configuracion principal, se espera el formato
 * 
 */
const iniciar = (configFile) => {
  let checkArchivoConfig = verificarArchivoConfig(configFile);
  if (checkArchivoConfig.finalizado == false) {
    return checkArchivoConfig;
  }
  let config = checkArchivoConfig.config;
  
  // inicializando cliente blockchain
  let fabric_client = new Fabric_Client();
  let channel = fabric_client.newChannel(config.channel_id);

  // cargando peers
  config.peers.forEach(peer => {
    let peer_tls_cacerts_data = fs.readFileSync(peer.peer_tls_cacerts);
    let nPeer = fabric_client.newPeer(peer.url,
                                      {
                                        pem: Buffer.from(peer_tls_cacerts_data).toString(),
                                        'ssl-target-name-override': peer.name
                                      }
                                     );
    channel.addPeer(nPeer);
  });

  // cargando orderers
  config.orderers.forEach(orderer => {
    let odata = fs.readFileSync(orderer.tls_ca_certs);
    let order = fabric_client.newOrderer(orderer.url,
                                         {
                                           pem: Buffer.from(odata).toString(),
                                           'ssl-target-name-override': orderer.name
                                         });
    channel.addOrderer(order);
  });

  // ajustes para fabric_client
  let blockchain = {
    memberUser: null,
    storePath: config.storePath,
    tx_id: null,
    resultado: {},
    resultadoConsulta: {},
    channel,
    Fabric_Client: Fabric_Client,
    fabric_client: fabric_client
  };

  return blockchain;
};

/**
 * Verifica estructura correcta del archivo de configuracion
 */
const verificarArchivoConfig = (configFile) => {
  if (!fs.existsSync(configFile)) {
    return {
      error: `No se encuentra archivo ${configFile}`,
      finalizado: false
    };
  }
  const config = require(configFile);
  // console.log('Contenido del archivo:::', config, '))))', typeof config);
  // comprobando datos CA
  console.log('datos ca...');
  try {
    if (!fs.existsSync(config.certificateAuthorities.tlsCACerts)) {
      return {
        error: `No se encuentra archivo ${config.certificateAuthorities.tlsCACerts}`,
        finalizado: false
      };
    }
  } catch (e) {
    console.log('CA error >>>', e);
    return {
      error: `Error buscando el archivo ${config.certificateAuthorities}: ${e}`,
      finalizado: false
    };
  }
  // comprobando orderers
  let errors = [];
  console.log('datos orderers...');
  try {
    config.orderers.forEach(orderer => {
      if (!fs.existsSync(config.orderers.tls_ca_certs)) {
        errors.push(`Error al buscar orderes ${config.peers}`);
      }
    });
  } catch (e) {
    console.log('Orderer error >>>', e);
    return {
      error: `Error al buscar orderes ${config.peers}: ${e}`,
      finalizado: false
    };
  }
  // comprobando datos Peers
  errors = [];
  console.log('datos peers...');
  try {
    config.peers.forEach(peerConfig => {
      if (!fs.existsSync(peerConfig.privateKeyFolder)) {
        errors.push(`Error al comprobar config.peers.privateKeyFolder no se encontró en: ${peerConfig.name} -> ${peerConfig.privateKeyFolder}`);
      }
      if (!fs.existsSync(peerConfig.signedCert) ) {
        errors.push(`Error al comprobar config.peers.signedCert no se encontró en: ${peerConfig.name} -> ${peerConfig.signedCert}`);
      }
      if (!fs.existsSync(peerConfig.peer_tls_cacerts)) {
        errors.push(`Error al comprobar config.peers.peer_tls_cacerts encontró en: ${peerConfig.name} -> ${peerConfig.peer_tls_cacerts}`);
      }
    });
  } catch (e) {
    console.log('peer error >>>', e);
    return {
      error: `Error al buscar peers ${config.peers}: ${e}`,
      finalizado: false
    };
  }

  if (errors.length > 0) {
    return {
      error: 'Errores encontrados: ' + JSON.stringify(errors),
      finalizado: false
    };
  }
  return {
    finalizado: true,
    config
  };
};

module.exports = function (configPath) {
  if (!configPath) {
    console.log('verificador blockchain: Error no se ha proporcionado archivo de configuración');
    return {
      finalizado: false,
      error: 'Error no se ha proporcionado archivo de configuración'
    };
  }

  let blockchainObj = iniciar(configPath);

  console.log("BLockchain Obj", blockchainObj);
  if (blockchainObj.error) {
    console.log(`[Blockchain] Error leyendo archivo de configuracion ${configPath}: ${blockchainObj.error}`);
    return null;
  }

  return blockchainObj;
};


